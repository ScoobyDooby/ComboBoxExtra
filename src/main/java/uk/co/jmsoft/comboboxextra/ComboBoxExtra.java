/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.comboboxextra;

import javax.swing.JComboBox;

/**
 * Adds select next/previous item methods to the standard JComboBox.
 * <p>
 * @author Scooby
 */
public class ComboBoxExtra extends JComboBox<Object>
{

    /**
     * Selects the next item in the combo box list. If there is less than 2
     * items in the list does nothing.
     * <p>
     * @param loopToStart - if true the first item will be selected when the end
     *                    of the list is reached. If false then it will remain
     *                    on the last item.
     */
    public void selectNextItem(boolean loopToStart)
    {
        if (this.getModel().getSize() > 1)
        {
            int currentIndex = this.getSelectedIndex();
            boolean atTheEnd = currentIndex == this.getModel().getSize() - 1;
            if (atTheEnd && loopToStart)
            {
                setSelectedIndex(0);
            }
            else
            {
                setSelectedIndex(currentIndex + 1);
            }

        }
    }

    /**
     * Selects the previous item in the list. If there is less than 2 items in
     * the list does nothing.
     * <p>
     * @param loopToEnd - if true will loop to the end of the list when on the
     *                  first item. If false will remain on the first item.
     */
    public void selectPreviousItem(boolean loopToEnd)
    {
        if (this.getModel().getSize() > 1)
        {
            int currentIndex = this.getSelectedIndex();
            boolean atTheStart = currentIndex == 0;
            if(atTheStart && loopToEnd)
            {
                setSelectedIndex(this.getModel().getSize() - 1);
            }
            else
            {
                setSelectedIndex(currentIndex - 1);
            }
        }
    }
}
