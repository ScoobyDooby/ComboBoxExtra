<h1>ComboBoxExtra</h1>
<p>An extension of javax.swing.JComboBox that adds selectNextItem() and selectPreviousItem() methods.
</p>